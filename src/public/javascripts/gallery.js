/**
 * Bind jQuery functionality for galleries
 */

$(document).ready(function() {

	/**
	 * DropZone config (jQuery instantiation doesn't seem to work, so set the
	 * options object here and let the standard Dropzone class selector happen)
	 */
	Dropzone.options.galleryDropzone = {
	paramName: "image", // The name that will be used to transfer the file
	maxFilesize: 5, // MB
	init: function () {
			// Event to handle uploads
			this.on("success", function (image) {
				// Use the new item's data to GET a new gallery-item div via AJAX - 
				// endpoint is specified with a data attr on the gallery div
				var gallery_image = JSON.parse(image.xhr.response);
				var gallery = $('#gallery');
		    	$.get(gallery.data("fetch-url") + '/' + gallery_image.id,
		    		null,
		    		function(new_item_html) {
		    			// Append the retrieved HTML and reload sortable to catch new item
		    			gallery.append(new_item_html);
		    			$(".sortable").sortable('reload');
		    		}
	    		);
		  	});
		}
	};

    /**
     * Handle sorting 
     */
	$(".sortable").sortable().bind("sortupdate", function(e, ui) {
		
		// Get an array of all items in the div, once the sort completes
	    var data_id_list = $(ui.endparent).children(".sortable-item").map(function () { 
	        return $(this).data("id");
	    }).get();

	    // Post ordered array to the endpoint (specified with a data attr on the parent div)
	    $.post($(ui.endparent).data("save-sort-url"), {id_list: data_id_list});
	});

	/**
	 * Enhance delete requests to go via AJAX and remove the deleted element from the page
	 */
	$('button.gallery-delete').on("click", function(event) {
		event.preventDefault();
		var delete_form = $(this).parent('form');
		var delete_div = $(delete_form).parents('.gallery-item');

		// Intercept the delete form and send via jQuery
		$.post(delete_form.attr("action"), delete_form.serialize()).done(function() {
			// Add some visual feedback on success
			$(delete_div).fadeTo("fast", 0.00, function(){ // Fade
				$(this).slideUp("slow", function() { // Slide up
					$(this).remove(); // Remove from the DOM
				});
			});
		});
	});
	    
});
