/**
 * Bind jQuery functionality to page elements
 */

$(document).ready(function() {

	// jQuery DataTables
	$.extend( $.fn.dataTable.defaults, {
	    ordering:  true,
	    stateSave: true
	} );
	$( '.data-table' ).DataTable();

	// Hide notification popups
	$('#messages-popup').fadeIn().delay(2000).fadeOut(2000);

	// Alerts Bootbox popups (with mark as read)
	$(document).on("click", ".bootbox-alerts-popup", function(e) {
		e.preventDefault();
		$.get($(this).data("read-url"));
		bootbox.alert($(this).data("message"));
	});

	// Bootstrap Filestyle
	$(":file").filestyle();

	// bxSlider
	$('.bxslider').bxSlider({
		adaptiveHeight: true,
		pagerCustom: '#bx-pager',
	});
});
