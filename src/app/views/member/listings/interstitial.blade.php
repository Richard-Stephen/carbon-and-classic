@extends('member.layout')

@section('title', "Listing review for {$advert->title}")

@section('content')

<div class="row">

	<div class="col-xs-12">

		<div class="jumbotron">

			<h2>{{ $advert->title }}</h2>
			@if ($advert->approval_status == Advert::ADVERT_PENDING)
				<p class="text-info">Your listing has been submitted for approval. You will be notified
				once it has been processed.</p>
			@elseif ($advert->approval_status == Advert::ADVERT_DRAFT)
				<p class="text-info">Your listing is currently a <i>draft</i>. 
				Follow the steps below to complete it and submit for approval.
				Please don't use your browser's <i>back</i>
				button to navigate - use the options below.</p>
			@elseif ($advert->approval_status == Advert::ADVERT_APPROVED)
				<p class="text-success">Your listing has been approved and is live on the site. You
				may edit it if you wish, but note that this <abbr title="Editing your listing's price, minimum offer price or postage options will not return it to draft status, so feel free to amend these whenever you need to."
				>may</abbr> return it to draft status and it would then need to be approved once again.</p>
				<p>{{ link_to_route('member.dashboard', 'Return to dashboard') }}</p>
			@else
				<p class="text-info">Your listing is being reviewed. It may required revision before
				it is displayed on the list. We will be in touch.</p>
			@endif

		</div>

	</div>

	<div class="col-xs-12 text-center text-info">
		<h2>What do I need to do now?</h2>
	</div>

	{{-- Edit --}}
	<div class="col-xs-12 col-sm-4">
		<div class="jumbotron">
			<span class="interstitial-step-numbers">1</span>
			<span class="glyphicon glyphicon-large glyphicon-pencil text-success"></span>
			<p class="interstitial-step-desc">Your listing text has been saved. You can 
			{{ link_to_route('member.listings.edit', 'edit it if you wish', [$advert->id]) }}.</p>
		</div>
	</div>

	{{-- Images --}}
	<div class="col-xs-12 col-sm-4">
		<div class="jumbotron">
			<span class="interstitial-step-numbers">2</span>
			@if ($advert->photos->isEmpty())
				<span class="glyphicon glyphicon-large glyphicon-camera text-danger"></span>
				<p class="interstitial-step-desc">You should
				{{ link_to_route('member.listings.gallery.index', 'add some photos', [$advert->id]) }} 
				to your listing to help it sell effectively.</p>
			@else 
				<span class="glyphicon glyphicon-large glyphicon-camera text-success"></span>
				<p class="interstitial-step-desc">You have added {{ $advert->photos()->count() }}
				{{ link_to_route('member.listings.gallery.index', 'photos', [$advert->id]) }}
				to this listing.</p>
			@endif
		</div>
	</div>

	{{-- Postage --}}
	<div class="col-xs-12 col-sm-4">
		<div class="jumbotron">
			<span class="interstitial-step-numbers">3</span>
			@if ($advert->postage->isEmpty())
				<span class="glyphicon glyphicon-large glyphicon-envelope text-danger"></span>
				<p class="interstitial-step-desc">You should add  some
				{{ link_to_route('member.listings.postage', 'postage options', [$advert->id]) }}
				to your listing. 
			@else
				<span class="glyphicon glyphicon-large glyphicon-envelope text-success"></span>
				<p class="interstitial-step-desc">You have added {{ $advert->postage->count() }} 
				{{ link_to_route('member.listings.postage', 'postage options', [$advert->id]) }}
				to your advert.</p>
			@endif
		</div>
	</div>

	@unless ($advert->approval_status == Advert::ADVERT_APPROVED 
		|| $advert->postage->isEmpty() || $advert->photos->isEmpty() )

		<div class="col-xs-12 text-center text-info">
			<h2>Ready?</h2>
		</div>

		<div class="col-xs-12">
			<div class="jumbotron">

				<p>{{ link_to_route(
					'member.listings.preview',
					'View a preview of my listing', 
					['id' => $advert->id], 
					[
						'class' => 'btn btn-primary center-block show-listing-preview-modal',
						'target' => '_blank',
					]
				) }}</p>

				@if ($advert->approval_status == Advert::ADVERT_PENDING)
				{{-- Already submitted, wait! --}}

					<p>You listing has already been submitted for approval. You do not need to do anything
					at the moment. You will be notified once it has been processed.
					{{ link_to_route('member.dashboard', 'Return to dashboard') }}.</p>
					
				@else
				{{-- Output approval button --}}

					<p>{{ link_to_route(
						'member.listings.finished', 
						'Submit my listing for approval',
						['id' => $advert->id],
						['class' => 'btn btn-success center-block']
					) }}</p>

				@endif

			</div>
		</div>

	@endunless

	</div> {{-- /.jumbotron --}}
</div> {{-- /.row --}}

@stop
