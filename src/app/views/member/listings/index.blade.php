@extends('member.layout')

@section('title', 'All Selling')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>All Selling</h1>
			</div>

			<div class="panel-body">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#currently-selling" aria-controls="currently-selling" role="tab" data-toggle="tab">Currently Selling</a></li>
					<li role="presentation"><a href="#completed-sales" aria-controls="completed-sales" role="tab" data-toggle="tab">Completed Sales</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">

					<div role="tabpanel" class="tab-pane fade in active" id="currently-selling">

						@if ($adverts->get('not_completed')->isEmpty())

							<div class="well">
								<p>You aren't currently selling anything. Why not 
								{{ link_to_route('member.listings.create', 'list something right now?') }}
								</p>
							</div>

						@else

							@include('member.listings._table', ['adverts' => $adverts->get('not_completed')])

						@endif
						
					</div>{{-- ./tab-pane --}}
					
					<div role="tabpanel" class="tab-pane fade" id="completed-sales">

						@if ($adverts->get('completed')->isEmpty())

							<div class="well">
								<p>You don't have any completed sales to display.</p>
							</div>

						@else

							@include('member.listings._table', ['adverts' => $adverts->get('completed')])

						@endif

					</div>{{-- ./tab-pane --}}
				
				</div>{{-- ./tab-content --}}

				{{ link_to_route('member.dashboard', 'Return to Dashboard', null, ['class' => 'btn btn-primary']) }}

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
