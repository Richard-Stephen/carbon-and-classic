@extends('member.layout')

@section('title', 'Update password')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>Update password</h1>
			</div>

			<div class="panel-body">

				<p>Enter a new password into both fields and click save.</p>

				{{ Former::open(route('member.doPassword'))->method('POST') }}

				{{ Former::password('password', 'New password') }}
				{{ Former::password('password_confirmation', 'Confirm new password') }}
				
				{{ Former::actions(
					Button::primary('Save')->submit(),
					Button::danger('Cancel')->asLinkTo(route('member.dashboard'))
				) }}

				{{ Former::close() }}

			</div> {{-- /.panel-body --}}


		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
