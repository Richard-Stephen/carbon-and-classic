<table class="table table-striped data-table" id="member-offers-paymentrequired">

	<thead>
		<tr>
			<th class="table-widget-fixed-column">{{-- Empty controls placeholder --}}</th>
			<th>{{-- Advert Title --}}</th>
			<th>Price</th>
			<th>Date</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
	
		@foreach($purchases as $offer)

				<tr>
					<td>{{-- Empty controls placeholder --}}</td>
					<td>{{ link_to_route('public.advert.archived', $offer->advert->title, ['slug' => $offer->advert->slug], ['target' => '_blank']) }}</td>
					<td>{{ $offer->gbp_offer_price_string }}</td>
					<td>{{ $offer->advert->winning_transaction->created_at->diffForHumans() }}</td>
					<td>{{ $offer->advert->purchase_status_string }}</td>
				</tr>

		@endforeach

	</tbody>
</table>
