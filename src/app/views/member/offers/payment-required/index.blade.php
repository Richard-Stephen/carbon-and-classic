@extends('member.layout')

@section('title', 'Payments required')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>Payments Required</h1>
			</div>

			<div class="panel-body">

				@if (! $payment_required->isEmpty())

					@include('member.offers.payment-required._table', $payment_required)

				@else

					<div class="well">
						None! Why not {{ link_to_route('home', 'find yourself something!') }}
					</div>

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
