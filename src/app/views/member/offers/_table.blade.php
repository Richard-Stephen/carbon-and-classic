<table class="table table-striped data-table" id="member-offers-index">

	<thead>
		<tr>
			<th>{{-- Advert Title --}}</th>
			<th>Advert Price</th>
			<th>My Offer</th>
			<th>Status</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($offers as $offer)

			<tr>
				<td>
					@if ($offer->advert->purchase_status == Advert::ADVERT_AVAILABLE && $offer->advert->approval_status == Advert::ADVERT_APPROVED)
						{{ link_to_route('public.advert', $offer->advert->title, ['slug' => $offer->advert->slug], ['target' => '_blank']) }} 
					@else 
						{{ link_to_route('public.advert.archived', $offer->advert->title, ['slug' => $offer->advert->slug], ['target' => '_blank']) }}
					@endif
				</td>
				<td>{{ $offer->advert->gbp_price_string }}</td>
				<td>{{ $offer->gbp_offer_price_string }}</td>
				<td>{{ $offer->acceptance_status_string }}</td>
			</tr>

		@endforeach

	</tbody>
</table>