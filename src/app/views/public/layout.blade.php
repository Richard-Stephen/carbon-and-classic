<!DOCTYPE html>

<html lang="en-GB">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="@yield('meta_description', Config::get('domain.site.default_meta_description'))">
		<title>
			@yield('title', Config::get('domain.site.default_page_title')) :: Carbon &amp; Classic
		</title>

		{{-- Load CSS --}}
		@foreach(array_merge($shared_css, [
			'/styles/compiled/public.css',
        ]) as $css_file)
			{{ HTML::style($css_file) }}
		@endforeach

		{{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>

		{{-- Bootstrap navbar if logged in, unless supressed (by a calling modal or similar) --}}
		@unless (Input::has('supress_navbar'))
			@include('nav.main')
		@endunless

		{{-- BS3 container --}}
		<div class="container-fluid">

			{{-- Show any notification messages --}}
			<div id="messages-popup">
				{{ Notification::showAll() }}
			</div>

			{{-- Main content --}}
			@yield('content')

		</div> {{-- ./container --}}

		{{-- Footer --}}
		@include('public.footer')

		{{-- Load Javascript --}}
		@foreach(array_merge($shared_js, [
			'/javascripts/public-bindings.js',
			'/javascripts/listings-bindings.js',
        ]) as $js_file)
			{{ HTML::script($js_file) }}
		@endforeach

	</body>
</html>
