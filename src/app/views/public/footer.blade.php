<footer>
	<div class="footer-content">
		<ul class="breadcrumb">
			<li>{{ link_to_route('page', 'Terms &amp; Conditions', ['slug' => 'terms-conditions']) }}</li>
			<li>Safe Purchasing Tips </li>
			<li>Partner Sites</li>
		</ul>
	</div>
</footer>

