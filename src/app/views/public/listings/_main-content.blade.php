{{-- Current category description --}}
<p>{{ $category->description }}</p>

{{-- Adverts in this category, if this is an advert container category--}}
@if ($category->is_advert_container)

	@if ($category->filtered_adverts->isEmpty())

		<p class="category-no-results">Your search has not returned any
		results. Try changing your search filters or picking a different
		category.</p>

	@else

		<div class="listing-list">

			@foreach ($category->filtered_adverts as $advert)

				@include('public.listings._advert-item')

			@endforeach

		</div>

	@endif

{{-- This is not an advert container category, so display subcategories --}}
@else

	@if ($category->subcategories->isEmpty())

		<p class="category-no-results">Your search has not returned any
		results. Try changing your search filters or picking a different
		category.</p>

	@else

		<div class="category-list">

			@foreach ($category->subcategories as $subcategory)
				
				@include('public.listings._subcategory-item')

			@endforeach

		</div> {{-- ./category-list --}}

	@endif 

@endif