{{-- Odd/even counter for lhs/rhs calc --}}
<?php $counter = 0 ?>

<section class="buying-home-main">

	<div class="buying-home-wrapper">

		@foreach ($category->subcategories as $subcategory)

			<?php $counter++; ?>

			<div class="specialism-outer">

				<div class="specialism-inner specialism-inner-{{ $counter & 1 ? 'lhs' : 'rhs' }}">

					<a href="{{ route('public.category', [$subcategory->slug]) }}">

						<img class="specialism-image" src="{{ $subcategory->image->url('large') }}" />

						<h2>{{ $subcategory->name }}</h2>

					</a>

				</div>

			</div>

		@endforeach

	</div>{{-- ./buying-home-wrapper --}}

</section>{{-- ./buying-home-main --}}