@extends('public.layout')

@section('content')

<section class="buying-home-main">

<div class="buying-home-wrapper">

	<div class="specialism-outer">

		<div class="specialism-inner specialism-inner-lhs">

			<a href="{{ route('public.category', ['modern']) }}">

				<img class="specialism-image" src="/images/homepage-carbon.jpeg" />

				<h2>Modern</h2>

			</a>

		</div>

	</div>

	<div class="specialism-outer">

		<div class="specialism-inner specialism-inner-rhs">

			<a href="{{ route('public.category', ['vintage']) }}">

				<img class="specialism-image" src="/images/homepage-classic.jpeg" />

				<h2>Vintage</h2>

			</a>

		</div>

	</div>

</div>

</section>

@stop
