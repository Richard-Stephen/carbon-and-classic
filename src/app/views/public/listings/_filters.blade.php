<section id="category-filters" class="category-search hidden">

	{{ Former::vertical_open(route('public.category', [$category->slug]))->id('filters-form') }}

	{{ Former::populate($search_filters->form_populate_data) }}

	{{-- Disable push_checkboxes so query filters work as designed --}}
	{{ Former::setOption('push_checkboxes', false) }}

		{{-- Define a flag we will use for an error message if no filters are output --}}
		<?php $filters_output_empty = true ?>

		{{-- Sorting (only display on advert pages) --}}
		@if ($category->is_advert_container)
			<div class="category-sort-adverts">
				{{ Former::select(Advert::SFKEY_SORT, 'Sort results by')->options($search_filters->advert_sort)->addClass('not-select2') }}
			</div>
		@endif
			
		{{-- Price slider, with hidden fields (default to min and max vals, ::populate will override) --}}
		@unless ($search_filters->advert_prices->min === $search_filters->advert_prices->max)
			
			<?php $filters_output_empty = false ?>

			{{ Former::hidden('start_price')
				->id('searchfilter-start-price')
				->value($search_filters->advert_prices->min) 
			}}
			{{ Former::hidden('end_price')
				->id('searchfilter-end-price')
					->value($search_filters->advert_prices->max)
			}}
			{{ Former::text($search_filters->advert_prices->slider_field_name, 'Price Range')
				->id('searchfilter-slider')
				->data_slider_min($search_filters->advert_prices->min) 
				->data_slider_max($search_filters->advert_prices->max)
				->data_slider_range("true")
			}}

			{{-- Placeholders for price range label --}}
			<div class="category-search-label">
				&pound;<span id="searchfilter-start-price-label">{{ 
					$search_filters->form_populate_data->get('start_price',
						$search_filters->advert_prices->min
					)
				}}</span>
				to
				&pound;<span id="searchfilter-end-price-label">{{
					$search_filters->form_populate_data->get('end_price',
						$search_filters->advert_prices->max
					)
				}}</span>
			</div>
		@endunless

		{{-- Manufactureres --}}
		@unless ( ! $search_filters->manufacturers)

			<?php $filters_output_empty = false ?>
			{{ Former::checkboxes('Manufacturers')->checkboxes($search_filters->manufacturers) }}
		
		@endunless

		{{-- Submit button, only if scipting is disabled --}}
		<noscript>
			<div class="category-search-buttons">
				{{ Former::actions()->large_primary_submit('Filter')->id('filter-submit-button') }}
			</div>
		</noscript>


		{{-- Show some helper text if nothing was output at all --}}
		@if ($filters_output_empty)
			<div class="no-filters-available">
				<p>No filters are available in this category</p>
			</div>
		@else
			{{-- Some filters apply, display the reset button --}}
			<p class="text-center">{{ link_to_route(
				'reset_category_search',
				'Reset filters',
				null,
				['class' => 'btn btn-default btn-sm']
			) }}</p>
		@endif

		{{-- Progress bar --}}
		<div id="filters-progress-bar">
			<img src="/images/loading-horizontal.gif" alt="Loading...">
		</div>

	{{ Former::close() }}
	
</section>