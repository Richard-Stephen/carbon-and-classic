@extends('public.layout')

@section('content')

<section class="home-main">

<div class="home-wrapper">


	<div class="logo">

		<img src="/images/logo.png" alt="Carbon &amp; Classic">

	</div>


	<div class="blurb">

		<p>The web's best place to sell your vintage and modern cycles and components. 
		Listing is free, with a simple fixed fee on successful sales.</p>
		<p>
			{{ link_to_route('users.create', 'Sign up', null, ['class' => 'btn btn-lg btn-primary']) }}
			{{ link_to_route('buying_home', 'Browse', null, ['class' => 'btn btn-lg btn-primary']) }}
		</p>

	</div>

</div>

</section>

@stop
