<section class="listing-images">

	@unless ($advert->photos->isEmpty())

		<div class="bx-container-custom">

			<div id="bx-controls-custom">
				<div id="bx-prev"></div>
				<div id="bx-next"></div>
				<div id="bx-magnify">

					{{-- Build magnify buttons and hide all but the first one --}}
					<?php $slider_index = 0 ?>
					@foreach ($advert->photos as $photo)
						<a id="bx-magnify-link-{{ $slider_index }}"
						href="{{ $photo->image->url('massive') }}" 
						data-lightbox="lightbox" 
						class="bx-magnify-links {{ $slider_index++ !== 0 ? 'hidden' : null }}">
							<span class="glyphicon glyphicon-zoom-in"></span>
						</a>
					@endforeach

				</div>
				<div id="bx-counter" class="bx-counter">1/{{ $advert->photos->count() }}</div>
			</div>

			<div id="bx-main-custom">

				{{-- Build full images --}}
				<ul class="bxslider">
					@foreach ($advert->photos as $photo)
						<li><img src="{{ $photo->image->url('massive') }}" /></li>
					@endforeach
				</ul>

				{{-- Build thumbnail pager --}}
				<div id="bx-pager">
					@foreach ($advert->photos as $n => $photo)
						<a data-slide-index="{{ $n }}" href="#"><img src="{{ $photo->image->url('thumb') }}" /></a>
					@endforeach
				</div>

			</div>{{-- ./bx-main-custom --}}

		</div>{{-- ./bx-container-custom --}}

	@endunless

</section>{{-- ./listing-images --}}


<section class="listing-body">

	<section class="listing-infobox">

		<div>

			<div class="listing-title">

				{{-- Mfr and advert title --}}
				<h2>{{ $advert->title }}</h2>

			</div>

			<div class="listing-location-and-price">

				<p>{{ $advert->location_string }}</p>
				<p>{{ $advert->gbp_price_string }}</p>

			</div>

		</div>
	
	</section>{{-- ./listing-infobox --}}

	<section class="listing-free-text">

		{{ $advert->body }}

	</section> {{-- ./listing-advert-body --}}

	<section class="listing-postage-options">

		{{-- Get applicable options if logged in, or all options if not (or if in preview mode --}}
		@if ((isset($preview_mode) && $preview_mode === true) || ! Confide::user())
				<?php $postage = $advert->postage ?>
		@else
				<?php $postage = $advert->user_applicable_postage_options ?>
		@endif

		{{-- Iterate and display postage --}}
		@if ($postage->count())

			<h3>Postage options</h3>

			<ul>

				@foreach($postage as $postage_option)

					@if ($postage_option->region_name == PostageOption::COLLECT_IN_PERSON)
						{{-- Special format for collect in person --}}
						<li>Collect in person</li>
					@else
						{{-- Normal collection option output --}}
						<li>{{ $postage_option->region_name }}:
							{{ $postage_option->gbp_price_string }}
							<small>({{ $postage_option->description }})</small>
						</li>
					@endif

				@endforeach

			</ul>

		@else

			<p class="text-danger">This advert has not been made available for delivery
			to your region. {{ link_to_route('messages.create', 'Message the seller', $advert->owner->username) }}
			to discuss - they may be willing to deliver to you, and can update this advert
			with applicable postage options on request.</p>

		@endif

	</section>


</section>{{-- ./listing-body --}}


