@extends('admin.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">
		
		<h1>Members</h1>

		<div class="alert alert-info" role="alert">
			<p>Members can be toggled between <strong>Admin</strong>, <strong>Member</strong> and
			<strong>Deactivated</strong> here.</p>
			<p>Members cannot be <strong>Deleted</strong>, so <strong>Deactivate</strong> them to
			make them disappear. This will stop them from logging in or using the site in any way.
			This will not affect their listings - you should deal with these manually (contacting
			buyers as appropriate).</p>
		</div>

		<div class="panel panel-primary">

			<div class="panel-body">

				@if ($users->isEmpty())

					<div class="alert alert-warning" role="alert">
						<p>No members found</p>
					</div>

				@else 

					<table class="table table-striped data-table" id="admin-members-index">
						<thead>
							<tr>
								<th data-orderable="false"></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($users as $user)
							<tr>
								<td>

									{{-- Manage dropdown --}}
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-label="Manage Item">
											<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
										</button>
										<ul class="dropdown-menu" role="menu">
											<li>
												{{ Form::open(array('route' => array('messages.create', $user->username), 'method' => 'GET')) }}
												    <button type="submit" class="btn btn-link">Message Seller</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('admin.members.promote', $user->id), 'method' => 'POST')) }}
												    <button type="submit" class="btn btn-link">Make Admin User</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('admin.members.demote', $user->id), 'method' => 'POST')) }}
												    <button type="submit" class="btn btn-link">Make Normal User</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('admin.members.deactivate', $user->id), 'method' => 'POST')) }}
												    <button type="submit" class="btn btn-link">Deactivate</button>
												{{ Form::close() }}
											</li>
										</ul>
									</div> {{--- ./Manage dropdown --}}

								</td>

								<td><strong>{{ $user->username }}</strong></td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->roles_list }}</td>
								<td>{{ $user->confirmed ? 'Confirmed' : 'Unconfirmed' }}</td>

							</tr>
						@endforeach
						</tbody>
					</table>

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop