{{ Former::populate($category) }}
{{ Former::withRules($rules) }}

{{ Former::select('parent_id', 'Parent Category')->options(['N/A'] + $categories) }}
{{ Former::text('name') }}
{{ Former::textarea('description')->rows(3) }}
{{ Former::checkbox('is_advert_container', 'Allow members to list adverts in this category')->check() }}
{{ Former::file('image') }}

{{-- Show current image section if exists --}}
@if ($category->image->size())
	<div class="form-group">
		<label class="control-label col-lg-2 col-sm-4" for="image">Current image</label>
		<div class="col-lg-10 col-sm-8">
			<img src="{{ $category->image->url('thumb') }}" class="pull-left thumbnail">
			<p class="text-muted text-right">You can upload a new image above, which will replace this one.</p>
		</div>
	</div>
@endif

{{ Former::actions(
	Button::primary('Save')->submit(),
	Button::danger('Cancel')->asLinkTo(route('admin.categories.index'))
) }}
