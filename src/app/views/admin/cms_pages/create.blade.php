@extends('admin.cms_pages._form')

@section('main')

<div class="panel-heading">
	<h1>Add CMS Page</h1>
</div>

<div class="panel-body">

	{{ Former::open(route('admin.cms-pages.store'))->method('POST') }}

		@include('admin.cms_pages._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop