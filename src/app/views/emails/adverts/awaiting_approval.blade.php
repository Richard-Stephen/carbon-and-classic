<h1>Congratulations {{ $profile->full_name }}, your advert has been added and is awaiting approval!</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->gbp_price_string }}</p>

<p>The advert will be reviewed by out team prior to going live. You will receive a
notification once this has taken place.</p>

<p>If you need to {{ link_to_route('member.listings.edit', 'make changes to your advert', [$advert->id]) }},
please note that it will be temporarily unavailable while it is approved once again.</p>
