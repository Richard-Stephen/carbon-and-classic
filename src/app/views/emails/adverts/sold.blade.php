<h1>Congratulations, your item has been sold and payment processed!</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->winning_transaction->gbp_purchase_price_string }}</p>

<p>You should contact the buyer as soon as possible to complete delivery.</p>

<h2>Buyer's contact details:</h2>
<p>
	{{ $advert->winning_transaction->buyer->profile->full_name }}<br>
	{{ implode('<br>', $advert->winning_transaction->buyer->profile->address_components) }}
</p>

<p>Thanks for using Carbon and Classic!</p>