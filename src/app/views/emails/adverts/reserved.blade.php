<h1>Congratulations, your item has been reserved and is awaiting payment!</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->winning_offer->gbp_offer_price_string }}</p>

<p>Thanks for using Carbon and Classic!</p>