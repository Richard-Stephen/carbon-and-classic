@extends('messenger.layout')

@section('title', $thread->subject)

@section('content')

    <div class="col-md-10 col-md-offset-1">
        <h1>{{ $thread->subject }}</h1>

        @foreach ($thread->messages as $message)
            <div class="media">
                <span class="pull-left">
                    @if ($message->user->profile)
                        <img src="{{ $message->user->profile->avatar->url('thumb') }}"
                        alt="{{ $message->user->first_name }}" class="thumbnail"/>
                    @endif
                </span>
                <div class="media-body">
                    <h5 class="media-heading">{{ $message->user->username }}</h5>
                    <p>{{ $message->body }}</p>
                    <div class="text-muted"><small>Posted {{ $message->created_at->diffForHumans() }}</small></div>
                </div>
            </div>
        @endforeach

        <h2>Add a new message</h2>
        
        {{Form::open(['route' => ['messages.update', $thread->id], 'method' => 'PUT'])}}

        <!-- Message Form Input -->
        <div class="form-group">
            {{ Form::textarea('message', null, ['class' => 'form-control textarea-simple']) }}
        </div>

        <!-- Submit Form Input -->
        <div class="form-group">
            {{ Form::submit('Submit', ['class' => 'btn btn-primary form-control']) }}
        </div>

        {{ Form::close() }}
        
    </div>

@stop
