@extends('public.layout')

@section('title', 'An error occurred')

@section('content')

<section class="error-page-body">
	
	<div class="error-page-image">
		<img src="/images/uh-oh.jpg" alt="Uh oh!">
	</div>
	
	<div class="error-page-text">
		<h1>Sorry - something has gone wrong</h1>
		
		<p>If you tried to view, purchase or make on offer on a listing,
		it looks like you are too late and it has been purchased already!
		Better luck next time.</p>
		
		<p>Otherwise, you may have tried to visit a page which no longer exists.
		Please visit our {{ link_to_route('home', 'homepage') }}, or use the search
		bar at the top of the page to find what you were looking for.</p>
	</div>
	
</section>

@stop
