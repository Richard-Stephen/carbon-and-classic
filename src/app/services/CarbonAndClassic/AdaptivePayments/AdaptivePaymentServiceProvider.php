<?php

namespace CarbonAndClassic\AdaptivePayments;

use Illuminate\Support\ServiceProvider;

class AdaptivePaymentsServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'adaptivepayments' instance container to our AdaptivePayments object
        $this->app['adaptivepayments'] = $this->app->share(function($app) {
            return new AdaptivePayments;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function() {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias(
                'AdaptivePayments',
                'CarbonAndClassic\AdaptivePayments\Facades\AdaptivePayments'
            );
        });
    }
}
