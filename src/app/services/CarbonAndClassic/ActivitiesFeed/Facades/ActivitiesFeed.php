<?php

namespace CarbonAndClassic\ActivitiesFeed\Facades;

use Illuminate\Support\Facades\Facade;

class ActivitiesFeed extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'activitiesfeed';
    }
}
