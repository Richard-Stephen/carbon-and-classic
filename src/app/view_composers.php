<?php

/*
|--------------------------------------------------------------------------
| View Composer
|--------------------------------------------------------------------------
|
| Centralised binding of additional data needed in views
|
*/

View::composer(array('admin.layout','member.layout','public.layout','public.layout-popup'), function($view)
{
    $view->with('shared_css', [
        '/bower_components/bootstrap/dist/css/bootstrap.min.css',
        '/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
        '/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css',
        '/bower_components/dropzone/dist/min/dropzone.min.css',
        '/bower_components/bxslider-4/dist/jquery.bxslider.css',
        '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css',
        '//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css',
        '/bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css',
        '/bower_components/lightbox2/dist/css/lightbox.css',
    ]);

    $view->with('shared_js', [
        '/bower_components/jquery/dist/jquery.min.js',
        '/bower_components/bootstrap/dist/js/bootstrap.min.js',
        '/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js',
        '/bower_components/dropzone/dist/min/dropzone.min.js',
        '/bower_components/html.sortable/dist/html.sortable.min.js',
        '/bower_components/bootbox/bootbox.js',
        '/bower_components/bxslider-4/dist/jquery.bxslider.min.js',
        '/bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js',
        '/bower_components/underscore/underscore-min.js',
        '/bower_components/javascript-equal-height-responsive-rows/grids.min.js',
        '/bower_components/jquery-backstretch/jquery.backstretch.min.js',
        '/bower_components/lightbox2/dist/js/lightbox.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js',
        '//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js',
        '//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js',
        '//ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js',
        '/javascripts/jquery-select-hierarchy/lib/js/jquery.select-hierarchy.js',
        '/javascripts/google-fonts.js',
        '/javascripts/bootstrap-filestyle.min.js',
        '/javascripts/gallery.js',
        '/javascripts/shared-bindings.js',
    ]);
});