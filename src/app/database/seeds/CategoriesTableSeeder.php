<?php

class CategoriesTableSeeder extends Seeder
{

    public function run()
    {

        // Make the roots
        $baum_vintage = Category::create(['name' => 'Vintage']);
        $baum_vintage->save();
        $baum_modern = Category::create(['name' => 'Modern']);
        $baum_modern->save();


       // Add in the predefined categories heirachy

        $vintage = [

            ['name' => 'Bicycles',
                'children' => [
                    ['name' => 'Racing', 'is_advert_container' => true],
                    ['name' => 'Touring, Randonneur', 'is_advert_container' => true],
                    ['name' => 'Track, Fixed Wheel', 'is_advert_container' => true],
                    ['name' => 'Tandems, Trikes, Recumbants', 'is_advert_container' => true],
                    ['name' => 'Roadster, Leisure', 'is_advert_container' => true],
                    ['name' => 'Mountain Bikes', 'is_advert_container' => true],
                    ['name' => '19th Century, Ordinaries, Safety Bikes, Early Trikes & Quads', 'is_advert_container' => true],
                ],
            ],
            ['name' => 'Components',
                'children' => [
                    ['name' => 'Frames', 'is_advert_container' => true],
                    ['name' => 'Wheels, Hubs, Rims, Tyres & Tubs', 'is_advert_container' => true],
                    ['name' => 'Chainsets, Bottom Brackets, Drivetrain', 'is_advert_container' => true],
                    ['name' => 'Gear Derailleurs, Levers', 'is_advert_container' => true],
                    ['name' => 'Brakes, Levers', 'is_advert_container' => true],
                    ['name' => 'Bars, Stems, Headsets', 'is_advert_container' => true],
                    ['name' => 'Saddles, Seatpins', 'is_advert_container' => true],
                    ['name' => 'Pedals, Toeclips & Straps, Shoes', 'is_advert_container' => true],
                    ['name' => 'Saddlebags, Panniers, Lights', 'is_advert_container' => true],
                    ['name' => 'Clothing, Shoes, Rollers & Trainers', 'is_advert_container' => true],
                    ['name' => 'Groupsets', 'is_advert_container' => true],
                ],
            ],
        ];

        $baum_vintage->makeTree($vintage);

        $modern = [
            ['name' => 'Bicycles',
                'children' => [
                    ['name' => 'Racing', 'is_advert_container' => true],
                    ['name' => 'Touring, Randonneur', 'is_advert_container' => true],
                    ['name' => 'Track, Fixed Wheel', 'is_advert_container' => true],
                    ['name' => 'Tandems, Trikes, Recumbants', 'is_advert_container' => true],
                    ['name' => 'Roadster, Leisure', 'is_advert_container' => true],
                    ['name' => 'Mountain Bikes', 'is_advert_container' => true],
                ],
            ],
            ['name' => 'Components',
                'children' => [
                    ['name' => 'Frames', 'is_advert_container' => true],
                    ['name' => 'Wheels, Hubs, Rims, Tyres & Tubs', 'is_advert_container' => true],
                    ['name' => 'Chainsets, Bottom Brackets, Drivetrain', 'is_advert_container' => true],
                    ['name' => 'Gear Derailleurs, Levers', 'is_advert_container' => true],
                    ['name' => 'Brakes, Levers', 'is_advert_container' => true],
                    ['name' => 'Bars, Stems, Headsets', 'is_advert_container' => true],
                    ['name' => 'Saddles, Seatpins', 'is_advert_container' => true],
                    ['name' => 'Pedals, Cleats, Shoes', 'is_advert_container' => true],
                    ['name' => 'Saddlebags, Panniers, Lights', 'is_advert_container' => true],
                    ['name' => 'Clothing, Rollers & Turbo Trainers', 'is_advert_container' => true],
                    ['name' => 'Groupsets', 'is_advert_container' => true],
                ],
            ],
        ];

        $baum_modern->makeTree($modern);
    }
}
