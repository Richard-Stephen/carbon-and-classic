<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('paypal_id', 255);

            $table->string('full_name', 255);
            $table->string('address_line_1', 255);
            $table->string('address_line_2', 255);
            $table->string('address_line_3', 255);
            $table->string('city', 100);
            $table->string('country', 2);
            $table->string('telephone', 20);

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
