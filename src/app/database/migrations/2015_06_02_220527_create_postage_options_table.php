<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostageOptionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('postage_options', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableTimestamps();

            $table->integer('advert_id')->unsigned()->index();
            $table->foreign('advert_id')->references('id')->on('adverts')->onDelete('cascade');

            $table->string('region_name');
            $table->string('description');
            $table->decimal('price', 7, 2);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('postage_options');
    }
}
