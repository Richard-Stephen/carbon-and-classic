<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSequenceFieldToGalleryPictures extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gallery_pictures', function(Blueprint $table)
		{
			$table->integer('sequence')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gallery_pictures', function(Blueprint $table)
		{
			$table->dropColumn('sequence');
		});
	}

}
