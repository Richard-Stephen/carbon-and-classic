<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('active');
            $table->tinyInteger('approval_status')->unsigned;

            $table->string('title', 255);
            $table->text('body');

            $table->decimal('price', 7, 2);
            $table->decimal('min_offer_price', 7, 2)->nullable();
            $table->decimal('views')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');

            $table->integer('manufacturer_id')->unsigned();
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('adverts');
    }
}
