<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('adverts');

            $table->integer('initiator_id')->unsigned()->nullable();
            $table->foreign('initiator_id')->references('id')->on('users');

            $table->decimal('offer_price', 7, 2);
            $table->tinyInteger('acceptance_status')->unsigned()->nullable();
            $table->string('acceptance_code')->nullable()->unique();
            $table->timestamp('expires_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('offers');
    }
}
