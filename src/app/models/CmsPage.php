<?php

// Composer
use Baum\Node;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class CmsPage extends Node implements SluggableInterface
{

    use ValidatingTrait;

    use SoftDeletingTrait;

    use SluggableTrait;

    use SearchableTrait;

    protected $fillable = [
        'title',
        'body',
        'redirect_url',
        'meta_description',
        'active',
        'nav_hidden',
        'parent_id'
    ];

    protected $sluggable = [
        'build_from' => 'title',
    ];

    protected $rules = [
        'parent_id' => ['integer'],
        'title' => ['required', 'max:255'],
        'body' => ['required', 'max:21844'],
        'active' => ['integer'],
        'nav_hidden' => ['integer'],
        'redirect_url' => ['max:255'],
        'meta_description' => ['max:255'],
    ];

    protected $searchable = [
        'columns' => [
            'title' => 10,
            'body' => 5,
        ],
    ];


    public function save(array $options = array())
    {
        // TODO! Wrap in exception handling to deal with nested set exceptions gracefully,
        // add a custom validation rule to use during validation, prune the list of
        // parents so invalid items are not presented
        return parent::save($options);
    }


    ///////////////////
    // Relationships //
    ///////////////////


    /**
     * Define polymorphic relationship with gallery photos (always sort by sequence)
     *
     * @return Collection
     */
    public function photos()
    {
        return $this->morphMany('GalleryPicture', 'imageable')->orderBy('sequence');
    }


    ////////////
    // Scopes //
    ////////////


    /**
     * Scope to limit results on public pages
     * @param Query $query
     * @return Query
     */
    public function scopeFrontendVisible($query)
    {
        return $query->whereActive(true)->whereNavHidden(false);
    }


    ///////////////
    // Accessors //
    ///////////////


    /**
     * Return the next level of categories
     *
     * @return Collection
     */
    public function getChildPagesAttribute()
    {
        return $this->getImmediateDescendants();
    }


    /**
     * Return the immediate parent category
     *
     * @return Category
     */
    public function getParentPageAttribute()
    {
        return $this->getParent();
    }


    /**
     * Get first gallery image, or an empty placeholder
     *
     * @return GalleryPicture
     */
    public function getPrimaryPhotoAttribute()
    {
        if (! $this->photos->isEmpty()) {
            return $this->photos->first();
        } else {
            return new GalleryPicture;
        }
    }
    

    //////////////
    // Mutators //
    //////////////


    /**
     * Save any 0 or otherwise non integer vals as null to avoid nested set issues
     */
    public function setParentIdAttribute($value)
    {
        $value = intval($value) === 0 ? null : $value;
        $this->attributes['parent_id'] = $value;
    }
}
