<?php

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    // Def constants and a container for the base role types - a User should have 0::1 of these
    const ROLE_ADMIN = 'Admin';
    const ROLE_MEMBER = 'Member';
    public static $base_roles = [self::ROLE_ADMIN, self::ROLE_MEMBER];


    ///////////////////
    // Relationships //
    ///////////////////
    

    /**
     * Define relationship with any activities
     *
     * @return Collection
     */
    public function activities()
    {
        return $this->hasMany('Activity')->orderBy('created_at', 'desc');
    }
}
