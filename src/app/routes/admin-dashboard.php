<?php

/*
|--------------------------------------------------------------------------
| Admin
|--------------------------------------------------------------------------
|
| Routes to deal with admin dashboard pages
|
*/

Route::group(['prefix' => 'admin', 'before' => 'entrust_admin'], function() {

    Route::get('', ['as' => 'admin.dashboard',
        function() {
            return Redirect::route('admin.activities.index');
        }
    ]);

    // Bind models and define routes for our basic resource controllers

    Route::model('adverts', 'Advert');
    Route::get('adverts/{adverts}/preview', [
        'as' => 'admin.adverts.preview', 'uses' => 'Admin\AdvertsController@preview']
    );
    Route::resource('adverts', 'Admin\AdvertsController', ['except' => ['create', 'store']]);

    Route::model('cms-pages', 'CmsPage');
    Route::resource('cms-pages', 'Admin\CmsPagesController', ['except' => ['show']]);

    // Adverts galleries - uses advert binding defined above, and a new binding
    // for gallery pictures - existing ownership checks on the advert itself
    // will take care of ownership checks, no need for more here
    Route::group(['prefix' => 'cms-pages/{cms_pages}/gallery'], function() {

        Route::model('cms_pages_gal_pic', 'GalleryPicture');

        Route::get('', [
            'as' => 'admin.cms-pages.gallery.index',
            'uses' => 'Admin\CmsPagesGalleryController@index'
        ]);
        Route::get('{cms_pages_gal_pic}', [
            'as' => 'admin.cms-pages.gallery.fetch',
            'uses' => 'Admin\CmsPagesGalleryController@fetch'
        ]);
        Route::post('store', [
            'as' => 'admin.cms-pages.gallery.store',
            'uses' => 'Admin\CmsPagesGalleryController@store'
        ]);
        Route::post('save-sort', [
            'as' => 'admin.cms-pages.gallery.save_sort',
            'uses' => 'Admin\CmsPagesGalleryController@sort'
        ]);
        Route::match(['PUT', 'PATCH'], '{cms_pages_gal_pic}', [
            'as' => 'admin.cms-pages.gallery.update',
            'uses' => 'Admin\CmsPagesGalleryController@update'
        ]);
        Route::delete('{cms_pages_gal_pic}', [
            'as' => 'admin.cms-pages.gallery.destroy',
            'uses' => 'Admin\CmsPagesGalleryController@destroy'
        ]);
    });

    Route::model('manufacturers', 'Manufacturer');
    Route::resource('manufacturers', 'Admin\ManufacturersController', ['except' => 'show']);

    Route::model('categories', 'Category');
    Route::resource('categories', 'Admin\CategoriesController', ['except' => 'show']);

    Route::model('activities', 'Activity');
    Route::get('activities/update-all', [
        'as' => 'admin.activities.update_all', 'uses' => 'Admin\ActivitiesController@updateAll'
    ]);
    Route::get('activities/delete-all', [
        'as' => 'admin.activities.delete_all', 'uses' => 'Admin\ActivitiesController@destroyAll'
    ]);
    Route::get('activities/{activities}/mark-read', [
        'as' => 'member.activities.mark_read', 'uses' => 'Admin\ActivitiesController@markRead'
    ]);
    Route::resource('activities', 'Admin\ActivitiesController', ['only' => ['index', 'update', 'destroy']]);

    // Bind members (users) management controller

    Route::model('users', 'User');
    Route::get('members', ['as' => 'admin.members.index',
        'uses' => 'Admin\MembersController@index']);
    Route::post('members/promote/{users}', ['as' => 'admin.members.promote',
        'uses' => 'Admin\MembersController@promote']);
    Route::post('members/demote/{users}', ['as' => 'admin.members.demote',
        'uses' => 'Admin\MembersController@demote']);
    Route::post('members/deactivate/{users}', ['as' => 'admin.members.deactivate',
        'uses' => 'Admin\MembersController@deactivate']);

});
