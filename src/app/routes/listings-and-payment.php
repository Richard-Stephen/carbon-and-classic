<?php

/*
|--------------------------------------------------------------------------
| Listings and Payment
|--------------------------------------------------------------------------
|
| Routes to deal with categories, listings (adverts) and payments processing
|
*/

////////////////
// Categories //
////////////////

Route::get('/buy', ['as' => 'buying_home', 'uses' => 'ListingsController@index']);

// Reset search data
Route::get('/category/reset', [
    'as' => 'reset_category_search', 'uses' => 'ListingsController@reset'
]);

// Normal category search
Route::bind('category', function($value) {

    // Lookup and bind based on id (int passed) or slug (string passed)
    $lookup_field = is_numeric($value) ? 'id' : 'slug';
    return Category::where($lookup_field, '=', $value)->firstOrFail();

});

// Page content (handles both AJAX and regular requests)
Route::match(['GET', 'POST'], 'category/{category}', [
    'as' => 'public.category', 'uses' => 'ListingsController@category'
]);



/////////////
// Adverts //
/////////////

// Bind adverts for general use
Route::bind('advert', function($value) {

    // Lookup and bind based on id (int passed) or slug (string passed)
    $lookup_field = is_numeric($value) ? 'id' : 'slug';
    return Advert::where($lookup_field, '=', $value)->frontendVisible()->firstOrFail();

});


// Show
Route::get('advert/{advert}', ['as' => 'public.advert', 'uses' => 'ListingsController@advert']);

// Reserve 
Route::get('advert/{advert}/reserve', [
    'as' => 'checkout.reserve', 'uses' => '\Member\CheckoutController@makeAutoSuccessOffer', 'before' => 'entrust_member'
]);

// Make an offer
Route::post('advert/{advert}/offer', [
    'as' => 'offer.make', 'uses' => '\Member\CheckoutController@makeOffer', 'before' => 'entrust_member'
]);

// Bind archived adverts for the read only archive view
Route::bind('archived_adverts', function($value) {

    // Lookup and bind based on id (int passed) or slug (string passed)
    $lookup_field = is_numeric($value) ? 'id' : 'slug';
    return Advert::where($lookup_field, '=', $value)->archivedFrontendVisible()->firstOrFail();

});
Route::get('archive/{archived_adverts}', [
    'as' => 'public.advert.archived', 'uses' => 'ListingsController@archivedAdvert'
]);

////////////
// PayPal //
////////////


// Buy via an accepted offer

Route::bind('offer_by_acceptance_code', function($value) {
    return Offer::where('acceptance_code', '=', $value)->firstOrFail();
});

Route::get('redeem/{offer_by_acceptance_code}', [
    'as' => 'offer.redeem', 'uses' => '\Member\CheckoutController@start', 'before' => 'entrust_member|profile_holder'
]);
Route::post('redeem/{offer_by_acceptance_code}/confirmed', [
    'as' => 'offer.redeem.confirmed', 'uses' => '\Member\CheckoutController@process', 'before' => 'entrust_member|profile_holder'
]);

// Complete transaction

Route::bind('transaction', function($value) {
    return Transaction::where('paypal_tracking_id', '=', $value)->firstOrFail();
});

Route::get('transaction/{transaction}/cancel', ['as' => 'checkout.cancel', 'uses' => '\Member\CheckoutController@cancel']);
Route::get('transaction/{transaction}/complete', ['as' => 'checkout.complete', 'uses' => '\Member\CheckoutController@complete']);
Route::post('ipn', '\Member\CheckoutController@ipn');
