<?php

/*
|--------------------------------------------------------------------------
| Public
|--------------------------------------------------------------------------
|
| Routes to deal with normal public pages
|
*/


///////////////////////////////
// Homepage, public browsing //
///////////////////////////////


Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::bind('page', function($value) {

    // Lookup and bind based on id (int passed) or slug (string passed)
    $lookup_field = is_numeric($value) ? 'id' : 'slug';
    return CmsPage::where($lookup_field, '=', $value)->frontendVisible()->firstOrFail();

});

Route::get('page/{page}', ['as' => 'page', 'uses' => 'HomeController@page']);


////////////
// Search //
////////////

Route::get('search', ['as' => 'search', 'uses' => 'SearchController@siteSearch']);
Route::post('search', ['as' => 'search', 'uses' => 'SearchController@siteSearch']);


///////////////
// Messaging //
///////////////


Route::group(['prefix' => 'messages', 'before' => 'entrust_user'], function () {

    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create/{username}', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);

});
