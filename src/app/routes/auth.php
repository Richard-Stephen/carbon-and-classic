<?php

/*
|--------------------------------------------------------------------------
| Auth
|--------------------------------------------------------------------------
|
| Routes to deal with login etc.
|
*/

//////////////
// New user //
//////////////

Route::get('signup', ['as' => 'signup', 'uses' => 'UsersController@create']);
Route::get('users/create', ['as' => 'users.create', 'uses' => 'UsersController@create']);
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::post('users', 'UsersController@store');


///////////
// Login //
///////////

Route::get('login', ['as' => 'login', 'uses' => 'UsersController@login']);
Route::get('users/login', ['as' => 'users.login', 'uses' => 'UsersController@login']);

Route::get('users/login-and-return', [
    'as' => 'users.login_and_return', 'uses' => 'UsersController@loginAndReturn'
]);

Route::post('users/login', 'UsersController@doLogin');


////////////
// Logout //
////////////

Route::get('logout', ['as' => 'logout', 'uses' => 'UsersController@logout']);
Route::get('users/logout', ['as' => 'users.logout', 'uses' => 'UsersController@logout']);


/////////////////////
// Forgot password //
/////////////////////

Route::get('users/forgot_password', ['as' => 'users.forgot-password', 'uses' => 'UsersController@forgotPassword']);
Route::post('users/forgot_password', 'UsersController@doForgotPassword');


////////////////////
// Reset password //
////////////////////

Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
