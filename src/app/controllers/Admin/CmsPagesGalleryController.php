<?php

namespace Admin;

// Domain
use CmsPage;
use GalleryPicture;
use Confide;

// Core
use Input;
use Redirect;
use View;
use Response;
use \Illuminate\Database\Eloquent\Collection;

// Third Party
use Notification;

// Note that a CmsPagesGallery doesn't have its own model - the gallery items
// are retrived directly into the CmsPage model via a polymorphic relationship

class CmsPagesGalleryController extends \BaseController
{

    /**
     * Display all contained gallery items
     *
     * @param  CmsPage  $cms_page
     * @return Response
     */
    public function index(CmsPage $cms_page)
    {
        return View::make('admin.cms_pages.gallery', compact('cms_page'));
    }


    /**
     * Retrieve HTML for a specific item
     *
     * @param  CmsPage $cms_page
     * @param  GalleryPicture $photo
     * @return Response
     */
    public function fetch(CmsPage $cms_page, GalleryPicture $photo)
    {
        return View::make('admin.cms_pages._galleryitem', compact('cms_page', 'photo'));
    }


    /**
     * Store a new gallery item - Ajax endpoint
     *
     * @param  CmsPage  $cms_page
     * @return Response
     */
    public function store(CmsPage $cms_page)
    {
        $photo = new GalleryPicture(Input::all());
        if ($cms_page->photos()->save($photo)) {
            return Response::json($photo, 200);
        } else {
            return Response::json('Upload failed', 400);
        }
    }


    /**
     * Update sorting info for the gallery
     *
     * @param  CmsPage $cms_page
     * @return Response
     */
    public function sort(CmsPage $cms_page)
    {
        foreach (Input::get('id_list') as $sequence => $id) {
            // For each posted id, go via photos() relationship to silently skip
            // any attempt to manipulate images not assigned to this CmsPage
            if ($photo = $cms_page->photos()->where('id', '=', $id)->first()) {
                $photo->fill(['sequence' => $sequence])->save();
            }
            
        }

        return Response::json('Saved', 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  CmsPage  $cms_page
     * @return Response
     */
    public function update(CmsPage $cms_page)
    {
        // Update image meta, order etc.
        return Response::json('Not implemented', 400);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  CmsPage  $cms_page
     * @param  GalleryPicture $photo
     * @return Response
     */
    public function destroy(CmsPage $cms_page, GalleryPicture $photo)
    {
        $photo->delete();
        return Response::json('Deleted', 200);
    }
}
