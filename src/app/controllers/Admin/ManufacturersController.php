<?php

namespace Admin;

// Domain
use Manufacturer;

// Core
use Input;
use Redirect;
use View;

// Third Party
use Notification;

class ManufacturersController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /manufacturers
     *
     * @return Response
     */
    public function index()
    {
        $manufacturers = Manufacturer::all();
        return View::make('admin.manufacturers.index', compact('manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /manufacturers/create
     *
     * @return Response
     */
    public function create()
    {
        $manufacturer = new Manufacturer();
        $rules = $manufacturer->getRules();
        return View::make('admin.manufacturers.create', compact('manufacturer', 'rules'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /manufacturers
     *
     * @return Response
     */
    public function store()
    {
        return $this->handleSave(new Manufacturer);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /manufacturers/{id}/edit
     *
     * @param  Manufacturer  $manufacturer
     * @return Response
     */
    public function edit(Manufacturer $manufacturer)
    {
        $rules = $manufacturer->getRules();
        return View::make('admin.manufacturers.edit', compact('manufacturer', 'rules'));
    }

    /**
     * Update the specified resource in storage.
     * PUT /manufacturers/{id}
     *
     * @param  Manufacturer  $manufacturer
     * @return Response
     */
    public function update(Manufacturer $manufacturer)
    {
        return $this->handleSave($manufacturer);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /manufacturers/{id}
     *
     * @param  Manufacturer  $manufacturer
     * @return Response
     */
    public function destroy(Manufacturer $manufacturer)
    {
        $manufacturer->delete();
        Notification::success('Manufacturer deleted');
        return Redirect::route('admin.manufacturers.index');
    }

    /**
     * Called from the store and update methods to validate/save/redirect
     * @param  Manufacturer $manufacturer
     * @return Response
     */
    private function handleSave($manufacturer)
    {
        if ($manufacturer->fill(Input::all())->save()) {
            Notification::success('Manufacturer saved');
            return Redirect::route('admin.manufacturers.index');
        } else {
            return Redirect::back()->withErrors($manufacturer->getErrors())->withInput();
        }
    }
}
