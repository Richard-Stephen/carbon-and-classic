<?php

namespace Admin;

// Domain
use CmsPage;

// Core
use Input;
use Redirect;
use View;

// Third Party
use Notification;

class CmsPagesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cms_pages = CmsPage::all();
        return View::make('admin.cms_pages.index', compact('cms_pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $cms_page = new CmsPage();
        $cms_pages = CmsPage::getNestedList('title', null, '&rArr;');
        $rules = $cms_page->getRules();
        return View::make('admin.cms_pages.create', compact('cms_page', 'cms_pages', 'rules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        return $this->handleSave(new CmsPage);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  CmsPage  $cms_page
     * @return Response
     */
    public function edit(CmsPage $cms_page)
    {
        $cms_pages = CmsPage::getNestedList('title', null, '&rArr;');
        $rules = $cms_page->getRules();
        return View::make('admin.cms_pages.edit', compact('cms_page', 'cms_pages', 'rules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CmsPage  $cms_page
     * @return Response
     */
    public function update(CmsPage $cms_page)
    {
        return $this->handleSave($cms_page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  CmsPage  $cms_page
     * @return Response
     */
    public function destroy(CmsPage $cms_page)
    {
        $cms_page->delete();
        Notification::success('Page deleted');
        return Redirect::route('admin.cms-pages.index');
    }

    /**
     * Called from the store and update methods to validate/save/redirect
     *
     * @param  CmsPage $cms_page
     * @return Response
     */
    private function handleSave($cms_page)
    {
        if ($cms_page->fill(Input::all())->save()) {
            Notification::success('Page saved');
            return Redirect::route('admin.cms-pages.gallery.index', [$cms_page->id]);
        } else {
            return Redirect::back()->withErrors($cms_page->getErrors())->withInput();
        }
    }
}
