<?php

namespace Admin;

// Domain
use Category;

// Core
use Input;
use Redirect;
use View;

// Third Party
use Notification;

class CategoriesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /categories
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();
        return View::make('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /categories/create
     *
     * @return Response
     */
    public function create()
    {
        $category = new Category();
        $categories = Category::getNestedList('name', null, '&rArr;');
        $rules = $category->getRules();
        return View::make('admin.categories.create', compact('category', 'categories', 'rules'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /categories
     *
     * @return Response
     */
    public function store()
    {
        return $this->handleSave(new Category);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /categories/{id}/edit
     *
     * @param  Category  $category
     * @return Response
     */
    public function edit(Category $category)
    {
        $categories = Category::getNestedList('name', null, '&rArr;');
        $rules = $category->getRules();
        return View::make('admin.categories.edit', compact('category', 'categories', 'rules'));
    }

    /**
     * Update the specified resource in storage.
     * PUT /categories/{id}
     *
     * @param  Category  $category
     * @return Response
     */
    public function update(Category $category)
    {
        return $this->handleSave($category);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /categories/{id}
     *
     * @param  Category  $category
     * @return Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        Notification::success('Category deleted');
        return Redirect::route('admin.categories.index');
    }

    /**
     * Called from the store and update methods to validate/save/redirect
     * @param  Category $category
     * @return Response
     */
    private function handleSave($category)
    {
        if ($category->fill(Input::all())->save()) {
            Notification::success('Category saved');
            return Redirect::route('admin.categories.index');
        } else {
            return Redirect::back()->withErrors($category->getErrors())->withInput();
        }
    }
}
