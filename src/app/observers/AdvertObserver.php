<?php

class AdvertObserver
{
    ////////////////////////
    // Watch Model events //
    ////////////////////////


    /**
     * Set approval status attribute and cache isDirty() for post save processing
     *
     * @param  Advert $advert
     */
    public function saving($advert)
    {
        // Make sure a default status of draft is applied to new adverts
        if (! $advert->exists) {
            $advert->approval_status = Advert::ADVERT_DRAFT;
        }

        // Adverts with ONLY whitelisted changes retain their approval status
        $whitelist = Config::get('domain.advert.skip_approval_whitelist');

        // Also need to whitelist internal attributes
        $whitelist += [
            'id',
            'slug',
            'approval_status',
            'views',
            'user_id',
            'created_at',
            'updated_at',
            'deleted_at',
            'purchased_at',
            'reserved_at',
            'shipped_at',
        ];

        // Unless approval_status has already been touched during save (e.g.
        // default was applied above, or specified manually where permitted)
        // or only whitelisted attributes were changed, set to draft
        if (! $advert->isDirty('approval_status')
            && array_except($advert->getDirty(), $whitelist)
        ) {
            $advert->approval_status = Advert::ADVERT_DRAFT;
        }

        // Cache for postprocessing
        $advert->cacheGetDirty();

    }


    /**
     * After saving, check the cached getDirty() data and process as needed
     *
     * @param  Advert $advert
     */
    public function saved($advert)
    {
        if ($advert->cachedIsDirty('approval_status')) {
            $this->processApprovalStatus($advert);
        }

        /* Null check prevents sending notifications when these have been cleared */
         
        if ($advert->cachedIsDirty('reserved_at') && $advert->reserved_at !== null) {
            $this->processPurchasePending($advert);
        }
        if ($advert->cachedIsDirty('purchased_at') && $advert->purchased_at !== null) {
            $this->processPurchaseComplete($advert);
        }
        if ($advert->cachedIsDirty('shipped_at') && $advert->shipped_at !== null) {
            $this->processShipped($advert);
        }
    }


    /**
     * Cascade deletions to offers
     *
     * @param Advert $advert
     */
    public function deleting($advert)
    {
        $advert->offers()->delete();
    }


    /////////////////////////////
    // Take action if required //
    /////////////////////////////


    /**
     * Handle notification of advert approval status
     *
     * @param  Advert $advert
     */
    public function processApprovalStatus($advert)
    {
        // If this is draft status, do nothing
        if ($advert->approval_status === Advert::ADVERT_DRAFT) {
            return;
        }

        // Ensure we have owner and (sane) profile objects to use
        $owner = $advert->owner;
        $profile = $owner->profile ?: Profile::makeDummy();

        /* Awaiting approval */

        if ($advert->approval_status === Advert::ADVERT_PENDING) {

            // Send to the admin role - build payload, add notification
            $payload = [
                'title' => 'Advert requires approval',
                'body' => View::make('emails.adverts.pending', compact('advert'))->render(),
            ];
            ActivitiesFeed::addForRole(Role::ROLE_ADMIN, $payload, $send_mail = true);

            // Send to the owner - build payload, add notification
            $payload = [
                'title' => 'Your advert is awaiting approval',
                'body' => View::make(
                    'emails.adverts.awaiting_approval',
                    compact('advert', 'owner', 'profile')
                )->render(),
            ];
            ActivitiesFeed::add($owner, $payload, $send_mail = true);

            // Early exit
            return;
        }

        /* Post approval */

        // We will choose one each of these views and subject lines, based on the approval_status attribute
        $notification_views = [
            Advert::ADVERT_DECLINED_TEMP => 'emails.adverts.declined_temp',
            Advert::ADVERT_DECLINED_PERM => 'emails.adverts.declined_perm',
            Advert::ADVERT_APPROVED => 'emails.adverts.approved',
        ];
        $notification_titles = [
            Advert::ADVERT_DECLINED_TEMP => 'Your advert has been declined and requires amendment',
            Advert::ADVERT_DECLINED_PERM => 'Your advert has been permanently declined',
            Advert::ADVERT_APPROVED => 'Your advert has been approved',
        ];
        
        // Build payload, add notification with default recipient email send
        $payload = [
            'title' => $notification_titles[$advert->approval_status],
            'body' => View::make(
                $notification_views[$advert->approval_status],
                compact('advert', 'owner', 'profile')
            )->render(),
        ];

        ActivitiesFeed::add($owner, $payload, $send_mail = true);

        // Finished
        return;
    }


    /**
     * Handle notification of reservation (purchase is pending)
     *
     * @param Advert $advert
     */
    public function processPurchasePending($advert)
    {

        /* Buyer */

        // Get the buyer from the winning offer, add an extra email receipients
        $buyer = $advert->winning_offer->initiator;
        $email_overrides = [
            'to' => $advert->winning_offer->initiator->email,
            'bcc' => Config::get('domain.role_email.Admin'),
        ];

        // Build payload, add notification using overrides for email recipients
        $payload = [
            'title' => 'Your Carbon and Classic offer has been accepted and is awaiting payment',
            'body' => View::make('emails.adverts.payable', compact('advert'))->render(),
        ];

        ActivitiesFeed::add($buyer, $payload, $email_overrides);

        /* Seller */

        // Build payload, add notification with default email recipient
        $payload = [
            'title' => 'Your Carbon and Classic sale is awaiting payment',
            'body' => View::make('emails.adverts.reserved', compact('advert'))->render(),
        ];

        ActivitiesFeed::add($advert->owner, $payload, $email = true);
    }


    /**
     * Handle notification of purchase completion
     *
     * @param Advert $advert
     */
    public function processPurchaseComplete($advert)
    {

        /* Buyer */

        // Get the buyer from the transaction, add an extra email receipients
        $buyer = $advert->winning_transaction->buyer;
        $email_overrides = [
            'to' => $advert->winning_transaction->paypal_sender_email, // Always use PayPal supplied email
            'bcc' => Config::get('domain.role_email.Admin'),
        ];

        // Build payload, add notification using overrides for email recipients
        $payload = [
            'title' => 'Your Carbon and Classic purchase is complete',
            'body' => View::make('emails.adverts.purchased', compact('advert'))->render(),
        ];

        ActivitiesFeed::add($buyer, $payload, $email_overrides);

        /* Seller */

        // Build payload, add notification with default email recipient
        $payload = [
            'title' => 'Your Carbon and Classic sale is complete',
            'body' => View::make('emails.adverts.sold', compact('advert'))->render(),
        ];

        ActivitiesFeed::add($advert->owner, $payload, $email = true);
    }


    /**
     * Handle notification of shipping completion
     *
     * @param Advert $advert
     */
    public function processShipped($advert)
    {

        /* Buyer */

        // Get the buyer from the transaction, add an extra email receipients
        $buyer = $advert->winning_transaction->buyer;
        $email_overrides = [
            'to' => $advert->winning_transaction->paypal_sender_email, // Always use PayPal supplied email
            'bcc' => Config::get('domain.role_email.Admin'),
        ];

        // Build payload, add notification using overrides for email recipients
        $payload = [
            'title' => 'Your Carbon and Classic purchase has been shipped',
            'body' => View::make('emails.adverts.shipped', compact('advert'))->render(),
        ];

        ActivitiesFeed::add($buyer, $payload, $email_overrides);
    }
}
